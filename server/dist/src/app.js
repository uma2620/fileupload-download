"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const multer = require("multer");
const bodyParser = __importStar(require("body-parser"));
const app = express();
const DIR = '/home/aspire1086/Desktop/fileUpload-download/server/dist/uploads';
const router = express.Router();
const path = require('path');
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    next();
});
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + '.' + path.extname(file.originalname));
    }
});
let upload = multer({ storage: storage });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/api/upload', upload.single('file'), function (req, res) {
    if (!req.file) {
        console.log("No file received");
        return res.send({
            success: false
        });
    }
    else {
        console.log('file received successfully');
        return res.send({
            success: true,
            path: `http://localhost:3000/api/upload/${req.file.filename}`
        });
    }
});
app.get('/api', function (req, res) {
    res.end('file catcher example');
});
app.get('/api/upload/:file(*)', (req, res) => {
    var file = req.params.file;
    var fileLocation = path.join('/home/aspire1086/Desktop/fileUpload-download/server/dist/uploads', file);
    console.log(fileLocation);
    res.download(fileLocation, file);
});
const PORT = process.env.PORT || 3000;
app.listen(PORT, function () {
    console.log('Node.js server is running on port ' + PORT);
});
