import  express  =require('express');
import {Request,Response,NextFunction} from 'express'
import  multer =require('multer');
import * as bodyParser from 'body-parser'

const app = express();

const DIR = '/home/aspire1086/Desktop/fileUpload-download/server/dist/uploads';
const router = express.Router();
const path = require('path');



app.use(function (req:Request, res:Response, next:NextFunction) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    next();
  });

let storage = multer.diskStorage({
    destination: (req:Request, file:any, cb:any) => {
      cb(null, DIR);
    },
    filename: (req:Request, file:any, cb:any) => {
      cb(null, file.fieldname + '-' + Date.now() + '.' + path.extname(file.originalname));
    }
});
let upload = multer({storage: storage});
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended: true}));
app.post('/api/upload',upload.single('file'), function (req:Request, res:Response) {
  if (!req.file) {
      console.log("No file received");
      return res.send({
        success: false
      });
  
    } else {
      console.log('file received successfully');
      return res.send({
        success: true,
        path: `http://localhost:3000/api/upload/${req.file.filename}`
      })
    }
});





app.get('/api', function (req:Request, res:Response) {
    res.end('file catcher example');
  });
  app.get('/api/upload/:file(*)',(req, res) => {
    var file = req.params.file;
    var fileLocation = path.join('/home/aspire1086/Desktop/fileUpload-download/server/dist/uploads',file);
    console.log(fileLocation);
    res.download(fileLocation, file); 
  }); 
  const PORT:any = process.env.PORT || 3000;
   
  app.listen(PORT, function () {
    console.log('Node.js server is running on port ' + PORT);
  });